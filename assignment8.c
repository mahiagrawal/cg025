#include<stdio.h>
int main(){
    int n, beg, end, mid, search, p[6];
    n = 6;
    printf("Enter the first six prime numbers\n");
    for(int i =0;i<n;i++){
        scanf("%d",&p[i]);
    }
    printf("\nThe given array is\n");
    for(int i = 0;i<n;i++){
        printf(" %d ",p[i]);
    }
    search = 11;
    beg = 0;
    end = n-1;
    while(beg<=end){
    mid = (beg + end)/2;
        if(p[mid]<search)
            beg = mid +1;
        else if(p[mid]==search){
            printf("\n%d is found at location %d",search,mid);
            break;
        }
        else
            end = mid - 1;
    }
    return 0;
}
