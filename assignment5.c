#include<stdio.h>
#include<math.h>
int main(){
    int a,b,c,flag;
    float x1,x2,d;
    printf("enter a\n");
    scanf("%d",&a);
    printf("enter b\n");
    scanf("%d",&b);
    printf("enter c\n");
    scanf("%d",&c);
    d = (b*b)-(4*a*c);
    if(d==0){
        flag = 0;
    }else if(d>0){
        flag = 1;
    }else{
        flag = 2;
    }
    switch(flag){
    case 0:
        x1 = -b/2;
        x2 = x1;
        printf("The given equation have equal roots which is %f",x1);
        break;
    case 1:
        x1 = ((-b) + sqrt(d))/2;
        x2 = ((-b) - sqrt(d))/2;
        printf("The roots of the given equation are x1 = %f and x2 = %f",x1,x2);
        break;
    case 2:
        printf("The given equation does not have real roots");
        break;    
    }
    return 0;
}