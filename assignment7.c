#include<stdio.h>
int main()
{
    int sum = 0, n, s;
    printf("Enter the value of n\n");
    scanf("%d",&n);
    s = 2;
    for(int i = 1 ; i<=n ; i++){
        sum = sum + s*s;
        s = s+2;
    }
    printf("The sum of squares of first %d even numbers is %d", n,sum);
    return 0;
}
