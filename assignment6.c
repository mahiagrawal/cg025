#include<stdio.h>
int main(){
    int pos,n = 7;
    int even[10] = {2,4,6,8,12,14,16};
    printf("Array before insertion is: \n");
    for(int k = 0; k<n ; k++){
        printf(" %d ",even[k]);
        }
    pos = 4;
    n++;
    for(int i = n-2 ; i>=pos ; i--){
        even[i+1] = even[i];
    }
    even[pos] = 10;
    printf("\nArray after insertion is: \n");
    for(int j = 0 ; j<n ; j++){
        printf(" %d ",even[j]);
    }
    return 0;
}
