#include<stdio.h>
int main(){
		int first[20][20];
		int second[20][20];
		int sum[20][20];
		int difference[20][20];
	int row,col;
	printf("Enter the number of rows");
	scanf("%d",&row);
	printf("Enter the number of columns");
	scanf("%d",&col);
	printf("Enter the elements for first matrix\n");
	for (int i=0;i<row;i++){
		for(int j=0;j<col;j++){
			scanf("%d",&first[i][j]);
		}
	}
	printf("Enter the elements for second matrix\n");
	for (int i=0;i<row;i++){
		for(int j=0;j<col;j++){
			scanf("%d",&second[i][j]);
		}
	}
	printf("The sum of the entered matrices\n");
	for (int i=0;i<row;i++){
		for(int j=0;j<col;j++){
			sum[i][j]=first[i][j]+second[i][j];
			printf("%d\t",sum[i][j]);
		}
		printf("\n");
	}
	printf("The difference of the entered matrices\n");
	for (int i=0;i<row;i++){
		for(int j=0;j<col;j++){
			difference[i][j]=first[i][j]-second[i][j];
			printf("%d\t",difference[i][j]);
		}
		printf("\n");
	}
	return 0;
}
