#include<stdio.h>
void swapnum ( int *var1, int *var2 )
{
int tempnum ;
tempnum = *var1 ; 
*var1 = *var2 ;
*var2 = tempnum ;
}
int main( )
{
	int num1,num2;
printf("Enter the two numbers:\n");
scanf("%d %d",&num1,&num2);
printf("Before swapping:\n");
printf("First number is %d\n", num1);
printf("Second number is %d\n", num2);
 
swapnum(&num1,&num2);
printf("After swapping:\n");
 printf("First number is %d\n", num1);
  printf("Second number is %d\n", num2);
   return 0;
}
