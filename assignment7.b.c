#include<stdio.h>
int main(){
    int n;
    float sum = 0,term;
    printf("Enter the value of n\n");
    scanf("%d",&n);
    for(int i =1;i<=n;i++){
        term = (float)1/(i*i);
        sum = sum + term;
    }
    printf("The sum of the %d terms of the series 1/1^2 + 1/2^2 + 1/3^2.... is %f",n,sum);
    return 0;
}