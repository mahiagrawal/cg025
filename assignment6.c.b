#include<stdio.h>
int main(){
    int odd[10], n = 8,dpos;
    for(int i =0, j=1 ; i<n ; i++){
        odd[i] = j;
        j = j+2;
    }
    printf("Array before deleting the element is\n");
    for(int i = 0 ; i<n ; i++){
        printf(" %d ", odd[i]);
    }
    n--;
    dpos = 2;
    for(int i = dpos; i<=n ; i++){
        odd[i] = odd[i+1];
    }
    printf("\nArray after deleting the element is\n");
    for(int i = 0 ; i<n ; i++){
        printf(" %d ", odd[i]);    
    }
    return 0;
}